from flask import Flask, render_template, url_for, request, redirect
from neo4j import GraphDatabase
from markupsafe import escape
import re
import os
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE,SIG_DFL) 

class Cluster(object):

    def __init__(self, uri, user, password):
        self._driver = GraphDatabase.driver(uri, auth=(user, password), encrypted=False)

    def close(self):
        self._driver.close()

    def search_subclusters_transaction(self, cluster_name):
        with self._driver.session() as session:
            clusters = session.write_transaction(self._get_subclusters, cluster_name)
        return clusters

    def search_superclusters_transaction(self, cluster_name):
        with self._driver.session() as session:
            clusters = session.write_transaction(self._get_superclusters, cluster_name)
        return clusters

    @staticmethod
    def _get_subclusters(tx, cluster_name):
        read = tx.run("MATCH (:cluster {name:$cn}) <-[:IS_SUBCLUSTER]- (sc:cluster)"
                      "RETURN sc", cn=cluster_name)
        clusters = []
        for i in read:
            for node in i:
                clusters.append(dict(zip(node.keys(), node.values())))
        return clusters

    @staticmethod
    def _get_superclusters(tx, cluster_name):
        read = tx.run("MATCH (sc:cluster) <-[:IS_SUBCLUSTER]- (:cluster {name:$cn})"
                      "RETURN sc", cn=cluster_name)
        clusters = []
        for i in read:
            for node in i:
                clusters.append(dict(zip(node.keys(), node.values())))
        return clusters

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if 'cluster' in request.form:
            requested_cluster = request.form['cluster']
            return redirect('/subclusters/' + requested_cluster)
        elif 'scluster' in request.form:
            requested_cluster = request.form['scluster']
            return redirect('/superclusters/' + requested_cluster)
        elif 'pdb_id' in request.form:
            if request.form['biounit_no'] != "":
                requested_pdb_id = request.form['pdb_id']
                requested_biounit_no = requested_pdb_id + "_" + request.form['biounit_no']
                return redirect('/cluster/biounit_no/' + requested_biounit_no)
            else:
                requested_pdb_id = request.form['pdb_id']
                return redirect('/cluster/pdb_id/' + requested_pdb_id)
        elif 'cluster_id' in request.form:
            requested_cluster = request.form['cluster_id']
            return redirect('/cluster/info/' + requested_cluster)
        elif 'complex_id' in request.form:
            requested_complex = request.form['complex_id']
            return redirect('/complex/info/' + requested_complex)
        elif 'complex_vis' in request.form:
            requested_complex = request.form['complex_vis']
            return redirect('/complex/visualization/' + requested_complex)
    else:
        return render_template('index.html')

@app.route('/subclusters/<clusterID>', methods=['GET','POST'])
def subclusters(clusterID):
    if request.method == 'POST':
        return redirect('/')
    else:
        if re.match("^ve[0-9]{1,4}cl[0-9]{1,5}$", str(clusterID)) is None:
            return redirect('/error/1')
        cluster = Cluster('bolt://localhost:7687', 'neo4j', 'slaptazodis')
        subclusters = cluster.search_subclusters_transaction(escape(clusterID))
        return render_template('subclusters.html', cluster_id=escape(clusterID), subclusters=subclusters)

@app.route('/superclusters/<clusterID>', methods=['GET','POST'])
def superclusters(clusterID):
    if request.method == 'POST':
        return redirect('/')
    else:
        if re.match("^ve[0-9]{1,4}cl[0-9]{1,5}$", str(clusterID)) is None:
            return redirect('/error/1')
        cluster = Cluster('bolt://localhost:7687', 'neo4j', 'slaptazodis')
        superclusters = cluster.search_superclusters_transaction(escape(clusterID))
        return render_template('superclusters.html', cluster_id=escape(clusterID), superclusters=superclusters)

@app.route('/cluster/pdb_id/<pdbID>', methods=['GET','POST'])
def cluster_pdb(pdbID):
    if request.method == 'POST':
        return redirect('/')
    else:
        if re.match("^[0-9]{1}[0-9A-Za-z]{3}$", str(pdbID)) is None:
            return redirect('/error/2')
        pdb_id = escape(pdbID)
        clusters = str(os.popen("uniq data/pdbIdClusters.csv | awk -F',' '{if ($2 == \"" + pdbID + "\") print $1}'").read()).splitlines()
        if len(clusters) == 0:
            return redirect('/error/5') 
        return render_template('cluster_pdb.html', pdb_id=escape(pdbID), clusters=clusters)

@app.route('/cluster/biounit_no/<biounitNo>', methods=['GET','POST'])
def cluster_biounit(biounitNo):
    if request.method == 'POST':
        return redirect('/')
    else:
        if re.match("^[0-9]{1}[0-9A-Za-z]{3}_[0-9]{1,2}$", str(biounitNo)) is None:
            return redirect('/error/3')
        biounit_no = escape(biounitNo)
        cluster = str(os.popen("grep '" + str(biounit_no) + "' data/biounitNoClusters.csv | awk -F',' '{print $1}'").read())
        if len(cluster) == 0:
            return redirect('/error/4') 
        return render_template('cluster_biounit.html', biounit_no=escape(biounitNo), cluster=cluster)

@app.route('/cluster/info/<clusterID>', methods=['GET','POST'])
def cluster_info(clusterID):
    if request.method == 'POST':
        return redirect('/')
    else:
        if re.match("^ve[0-9]{1,4}cl[0-9]{1,5}$", str(clusterID)) is None:
            return redirect('/error/1')
        info = str(os.popen("tail -n +2 data/info_files/" + escape(clusterID) + "_info.txt").read()).splitlines()
        for i in range(len(info)):
            info[i] = info[i].split("\t")
        if len(info) == 0:
            return redirect('/error/6')
        return render_template('cluster_info.html', info=info)

@app.route('/complex/info/<complexID>', methods=['GET','POST'])
def complex_info(complexID):
    if request.method == 'POST':
        return redirect('/')
    else:
        if re.match("^[0-9]{1}[0-9A-Za-z]{3}_[0-9]{1,2}$", str(complexID)) is None:
            return redirect('/error/3')
        info = str(os.popen("grep --perl-regex '\t" + str(complexID) + "\t' data/ppi3d").read()).splitlines()
        for i in range(len(info)):
            info[i] = info[i].split("\t")
        if len(info) == 0:
            return redirect('/error/4')
        return render_template('complex_info.html', info=info)

@app.route('/complex/visualization/<complexID>', methods=['GET','POST'])
def complex_vis(complexID):
    if request.method == 'POST':
        return redirect('/')
    else:
        if re.match("^ve[0-9]{1,4}cl[0-9]{1,5}$", str(complexID)) is None:
            return redirect('/error/1')
        os.system("for i in static/cl_pictures/*; do rm -rf $i; done")
        os.system("CLUSTER_ID='" + str(escape(complexID)) + "'; python data/visualizePruning.py -o -l data/clusters/$(echo $CLUSTER_ID)/*")
        os.system("for i in data/clusters/*/*.svg; do mv $i static/cl_pictures; done")
        info = str(os.popen("ls static/cl_pictures").read()).splitlines()
        complex_names = []
        for i in range(len(info)):
            info[i] = "cl_pictures/" + info[i]
            complex_names.append(info[i][12:][:-4])
        if len(info) == 0:
            return redirect('/error/6')
        return render_template('complex_vis.html', info=info, cn=complex_names)

@app.route('/error/<codeNr>', methods=['GET','POST'])
def error(codeNr):
    if request.method == 'POST':
        return redirect('/')
    else:
        return render_template('error.html', code=codeNr)

if __name__ == "__main__":
    app.run(debug=True)

